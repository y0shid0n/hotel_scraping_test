# るるぶトラベルから各ホテルのページを取得する

import requests
from bs4 import BeautifulSoup
from selenium import webdriver
from pathlib import Path
import csv
import numpy as np

# 出力先
output_path = "./html/"

# seleniumの設定
chromeOptions = webdriver.ChromeOptions()

# ヘッドレスモードを有効にする
chromeOptions.add_argument('--headless')

# chromedriverの場所
#driver_location = config["default"]["driver_location"]
driver_location = "C:/temp/chromedriver_win32/chromedriver.exe"

# seleniumの起動
# chromeのバージョンに合わせたドライバを使う必要があるので適宜ダウンロードして使う
# https://chromedriver.chromium.org/downloads
driver = webdriver.Chrome(executable_path=driver_location
                          , chrome_options=chromeOptions)


# ベースのURL
base_url = "https://www.rurubu.travel/region/japan/"

# 取得したいホテルがある県のリスト
list_pref = ["fukuoka"]

# 出力用のリスト
list_output = [["target_id", "url"]]

def get_rrb_html(target_url, target, output_path, update = False):
    """
    県ごとのホテルのhtmlを取得
    """
    # urlを生成
    #target_url = base_url + target

    # 出力ファイルを生成
    target_html = output_path + "rrb_" + str(target) + ".html"

    # ToDo: update=Falseでも、ファイルがない場合はrequestを投げるべき
    if update or not Path(target_html).exists():
        # 全体のページを開く
        driver.get(target_url)

        # 文字コードをUTF-8に変換してから取得
        html = driver.page_source.encode('utf-8')
        soup = BeautifulSoup(html, "html.parser")
        print(html)

        # htmlを保存
        with open(target_html, "w", encoding="utf-8") as f:
            f.write(soup.prettify())
    else:
        with open(target_html, "r", encoding="utf-8") as f:
            soup = BeautifulSoup(f, "lxml")

    return soup

# 連続でアクセスするとエラーが返ってきてるっぽい
for pref in list_pref:
    target_url = base_url + pref
    soup = get_rrb_html(target_url, pref, output_path, update = False)
    hotels = soup.find_all("a", class_ = "HotelName__Link")
    hotels_url = [i.attrs['href'] for i in hotels]
    hotels_id = [i.attrs['data-hotel-id'] for i in hotels]

    tmp = [hotels_id, hotels_url]
    tmp = np.array(tmp).T.tolist()
    list_output += tmp

    # 出力
    with open("./output/rrb_hotel_url.csv", "w", encoding="utf-8") as f:
        writer = csv.writer(f)
        writer.writerows(list_output)
