# るるぶの各施設のページの情報を取得する

import pandas as pd
from pathlib import Path
from bs4 import BeautifulSoup
from selenium import webdriver
from urllib.parse import urljoin
from logging import getLogger, StreamHandler, Formatter

# create logger
logger = getLogger(__name__)
log_fmt = Formatter("%(asctime)s %(name)s %(lineno)s [%(levelname)s][%(funcName)s] %(message)s")

# ログレベル設定
log_level = "INFO"
logger.setLevel(log_level)

# handlerの生成
handler = StreamHandler()
handler.setLevel(log_level)
handler.setFormatter(log_fmt)
logger.addHandler(handler)


# 出力先
output_path = "./html/"

# seleniumの設定
chromeOptions = webdriver.ChromeOptions()

# ヘッドレスモードを有効にする
chromeOptions.add_argument('--headless')

# chromedriverの場所
#driver_location = config["default"]["driver_location"]
driver_location = "C:/temp/chromedriver_win32/chromedriver.exe"

# seleniumの起動
# chromeのバージョンに合わせたドライバを使う必要があるので適宜ダウンロードして使う
# https://chromedriver.chromium.org/downloads
driver = webdriver.Chrome(executable_path=driver_location
                          , chrome_options=chromeOptions)

# ベースのURL
base_url = "https://www.rurubu.travel/"

# urlのcsvを読み込む
df_url = pd.read_csv("./output/rrb_hotel_url.csv")

# 相対URLになっているのでベースURLと結合する
df_url["url"] = df_url["url"].apply(lambda x: urljoin(base_url, x))

# 施設コードをkey, URLをvalueとした辞書を作成
dict_url = df_url.set_index("target_id")["url"].to_dict()
logger.debug(dict_url)

# テスト用
cnt = 0

def get_rrb_html(target_url, target, output_path, update = False):
    """
    get_url_rrb_hotelの関数と同じだけどimportするとseleniumが変なことになるのでとりあえずこっちにコピーした
    """
    # urlを生成
    #target_url = base_url + target

    # 出力ファイルを生成
    target_html = output_path + "rrb_" + str(target) + ".html"

    # ToDo: update=Falseでも、ファイルがない場合はrequestを投げるべき
    if update or not Path(target_html).exists():
        # 全体のページを開く
        driver.get(target_url)

        # 文字コードをUTF-8に変換してから取得
        html = driver.page_source.encode('utf-8')
        soup = BeautifulSoup(html, "html.parser")
        print(html)

        # htmlを保存
        with open(target_html, "w", encoding="utf-8") as f:
            f.write(soup.prettify())
    else:
        with open(target_html, "r", encoding="utf-8") as f:
            soup = BeautifulSoup(f, "lxml")

    return soup


def get_rrb_info(soup):
    """
    るるぶの各施設ページから情報を取得する
    """
    # ホテル名
    hotel_name = soup.find_all("div", class_ = "HeaderCerebrum")
    hotel_name = [i.find("h1").text.strip() for i in hotel_name]
    return hotel_name


# keyごとに処理
for target_id, target_url in dict_url.items():
    logger.info("target: " + str(target_id))
    # htmlを取得
    soup = get_rrb_html(target_url, target_id, output_path, update=False)
    rrb_info = get_rrb_info(soup)
    logger.info(rrb_info)

    # テスト用
    cnt += 1
    if cnt > 0:
        break
