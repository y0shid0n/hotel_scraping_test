# 楽天トラベルAPIの地域コードで施設検索を実行

import requests
import json
import time
import yaml
from pathlib import Path

from logging import getLogger, StreamHandler, Formatter

# create logger
logger = getLogger(__name__)
log_fmt = Formatter("%(asctime)s %(name)s %(lineno)s [%(levelname)s][%(funcName)s] %(message)s")

# ログレベル設定
log_level = "INFO"
logger.setLevel(log_level)

# handlerの生成
handler = StreamHandler()
handler.setLevel(log_level)
handler.setFormatter(log_fmt)
logger.addHandler(handler)


# 施設検索APIのURL
target_url = "https://app.rakuten.co.jp/services/api/Travel/SimpleHotelSearch/20170426"

# config読み込み
with open("./conf/config.yml", mode='r', encoding="utf-8") as f:
    config = yaml.load(f, Loader=yaml.SafeLoader)

# ベースのパラメータ
params_base = {
    "applicationId": config["default"]["applicationId"]
    , "formatVersion": "2"
    , "largeClassCode": "japan"
    , "responseType": "large"
    }

# 地域コードを辞書として読み込む
with open("rakuten_travel_area.json", "r", encoding="utf-8") as f:
    dict_all_area = json.load(f)

# ネストの一番深い階層をループさせる
# smallClassまでは全ての地域に存在するが、detailClassは存在しない地域もある
# middleClass（県）以下のリストを取得
list_middle_classes = dict_all_area["areaClasses"]["largeClasses"][0][1]["middleClasses"]

# 各地域のparamを格納するリスト
list_area_params = []

# 各地域の最も深い地域コードを辞書で取得
for mc in list_middle_classes:
    params_tmp = {}

    # 複数ページの場合があるので1ページ目から始まるように指定しておく
    params_tmp["page"] = 1

    # 各県の下の階層を取得
    params_tmp["middleClassCode"] = mc["middleClass"][0]["middleClassCode"]
    list_small_classes_tmp = mc["middleClass"][1]["smallClasses"]

    for sc in list_small_classes_tmp:
        params_tmp["smallClassCode"] = sc["smallClass"][0]["smallClassCode"]
        # detailClassが存在する場合は取得する
        if len(sc["smallClass"]) == 2:
            list_detail_classes_tmp = sc["smallClass"][1]["detailClasses"]
            for dc in list_detail_classes_tmp:
                params_tmp["detailClassCode"] = dc["detailClass"]["detailClassCode"]
                list_area_params.append(params_tmp.copy()) # copyにしないと全ての要素が上書きされる
                del params_tmp["detailClassCode"] # 次のループに残らないように削除
        else:
            list_area_params.append(params_tmp.copy())

# paramsに各地域コードを追加してデータを取得
for area in list_area_params:
    # パラメータの初期化
    params = params_base.copy()

    # 対象地域のパラメータを追加
    params.update(area)
    logger.info(params)

    # ファイル名の生成
    if "detailClassCode" in params:
        file_name = "./output/{}_{}_{}_{}.json".format(params["middleClassCode"], params["smallClassCode"], params["detailClassCode"], "1")
    else:
        file_name = "./output/{}_{}_{}.json".format(params["middleClassCode"], params["smallClassCode"], "1")

    # ファイルが存在しない場合のみリクエスト実行（存在する場合はファイルを読み込む）
    # ToDo: ここは関数化する（2ページ目以降でも使うので）
    if Path(file_name).exists():
        with open(file_name, "r", encoding="utf-8") as f:
            output = json.load(f)
    else:
        time.sleep(2)
        r = requests.get(target_url, params=params)
        # httpステータスコードでエラーが帰ってきたら飛ばす
        if r.status_code == requests.codes.ok:
            output = json.loads(r.text)
            with open(file_name, "w", encoding="utf-8") as f:
                json.dump(output, f, ensure_ascii=False, indent=4)
        else:
            # ToDo: debugログで別のとこに出した方がいいかも
            logger.info("HTTP ERROR!!! status code: " + str(r.status_code))
            continue

    # pagingInfoに最大ページ数があるので、2ページ目以降が存在する場合はループで取得する
    page_num = output["pagingInfo"]["pageCount"]
    if page_num > 1:
        for i in range(2, page_num + 1):
            params.update({"page": str(i)})
            logger.info(params)

            # ファイル名の生成
            if "detailClassCode" in params:
                file_name = "./output/{}_{}_{}_{}.json".format(params["middleClassCode"], params["smallClassCode"], params["detailClassCode"], str(i))
            else:
                file_name = "./output/{}_{}_{}.json".format(params["middleClassCode"], params["smallClassCode"], str(i))

            # ファイルが存在しない場合のみ実行
            if not Path(file_name).exists():
                time.sleep(2)
                r = requests.get(target_url, params=params)
                # httpステータスコードでエラーが帰ってきたら飛ばす
                if r.status_code == requests.codes.ok:
                    output = json.loads(r.text)
                    with open(file_name, "w", encoding="utf-8") as f:
                        json.dump(output, f, ensure_ascii=False, indent=4)
                else:
                    # ToDo: debugログで別のとこに出した方がいいかも
                    logger.info("HTTP ERROR!!! status code: " + str(r.status_code))
