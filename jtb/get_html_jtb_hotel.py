# jtbのサイトからホテルのページのhtmlをDLする

import requests
from bs4 import BeautifulSoup
import csv
import re
from pathlib import Path
from time import sleep
from random import random
import logging
from logging import getLogger, StreamHandler, Formatter

# create logger
logger = getLogger(__name__)
log_fmt = Formatter("%(asctime)s %(name)s %(lineno)s [%(levelname)s][%(funcName)s] %(message)s")

# ログレベル設定
log_level = "INFO"
logger.setLevel(log_level)

# handlerの生成
handler = StreamHandler()
handler.setLevel(log_level)
handler.setFormatter(log_fmt)
logger.addHandler(handler)


# 出力先
output_path = "./html/"

# ベースのURL
base_url = "https://www.jtb.co.jp/kokunai_hotel/htl/"

# 取得したいホテルの施設コードのリスト
list_target_id = [8115102, 8115035, 8120008]

# 出力用のリスト
list_output = [["target_id", "jtb_score", "rrb_score", "hotel_name", "hotel_kana"
    , "hotel_address", "hotel_access", "hotel_price_min", "hotel_price_max"]]

def get_html(target_url, target_id, output_path, update = False):
    """
    target_idのhtmlを取得
    """
    # urlを生成
    #target_url = base_url + str(target_id)

    # 出力ファイル名を生成
    target_html = output_path + str(target_id) + ".html"

    # update=Falseでも、ファイルがない場合はrequestを投げる
    if update or not Path(target_html).exists():
        # htmlを取得
        r = requests.get(target_url)
        soup_tmp = BeautifulSoup(r.text, "lxml")

        # htmlを保存
        with open(target_html, "w", encoding="utf-8") as f:
            f.write(soup_tmp.prettify())
    else:
        with open(target_html, "r", encoding="utf-8") as f:
            soup_tmp = BeautifulSoup(f, "lxml")
    return soup_tmp

def get_target_info(soup):
    """
    パースされたhtmlから必要な情報を取得する
    評価のスコア：お客様アンケートとるるぶトラベルの2つあるのでとりあえず両方返す
    ホテル名：漢字とカナ
    住所、アクセス：とりあえず文字列そのまま
    料金：minとmax
    """
    # 評価のスコア
    # emタグがない場合はスコアがないので空文字にする
    scores = soup.find_all("p", class_ = "dom-rating__score")
    scores = ["" if i.find("em") is None else i.find("em").string.strip() for i in scores]
    # ホテル名
    hotel_name = soup.find("div", class_ = "domhotel-hotel-ttl__name")
    hotel_name_kanji = re.search("^.*(?=のアクセス)", hotel_name.find("h1").text.strip().replace("\u3000", "")).group(0)
    hotel_name_kana = hotel_name.find("span").text.strip().strip("（").strip("）")
    # 住所、アクセス
    # 基本情報の1要素目と2要素目をとる
    basic_info = soup.find("div", class_ = "dom-layout-03")
    basic_info = basic_info.find_all("section", class_ = "dom-section-02")
    basic_info = basic_info[0:2]
    basic_info = [i.find("p").text.strip() for i in basic_info]
    logger.debug(basic_info)
    # 料金のminとmax
    # たぶん料金が書いてないということはないでしょう（適当）
    # そんなことはなかった
    hotel_price = soup.find("div", class_ = "domhotel-total-price")
    try:
        hotel_price = [i.text.strip().replace(",", "") for i in hotel_price.find_all("em")]
    except AttributeError:
        hotel_price = ["", ""]
    return scores + [hotel_name_kanji] + [hotel_name_kana] + basic_info + hotel_price

# ループで処理
# ToDo: updateは引数で管理するほうがよい
for target in list_target_id:
    # htmlの取得
    logger.info("target:" + str(target))
    target_url = base_url + str(target)
    soup_tmp = get_html(target_url, target, output_path, update = False)
    logger.info("finish to get html")

    # 404の場合の処理
    judge_404 = soup_tmp.find("p", class_ = "heading-copy")
    if judge_404 is not None and judge_404.text.strip() == "404 Page not found.":
        logger.info("404 error")
        output_tmp = [target] + [""] * (len(list_output[0]) - 1)
    else:
        output_tmp = get_target_info(soup_tmp)
        output_tmp = [target] + output_tmp

    logger.debug(output_tmp)
    list_output += [output_tmp]

    sleep(5 + random())

# まとめて出力
logger.debug(list_output)
with open("./output/scores.csv", "w", encoding="utf-8", newline="") as f:
    writer = csv.writer(f)
    writer.writerows(list_output)
