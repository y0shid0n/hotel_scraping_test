# 施設検索APIの結果を読み込んでpandasで扱えるようにする

import pandas as pd
import json
from pathlib import Path
from multiprocessing import Pool

def create_df(hotel):
    """
    各ホテルの詳細情報をリストで受け取ってpandasにする
    """
    basic_info = pd.io.json.json_normalize(hotel[0]["hotelBasicInfo"])
    rating_info = pd.io.json.json_normalize(hotel[1]["hotelRatingInfo"])
    detail_info = pd.io.json.json_normalize(hotel[2]["hotelDetailInfo"])
    facilities_info = pd.io.json.json_normalize(hotel[3]["hotelFacilitiesInfo"])
    policy_info = pd.io.json.json_normalize(hotel[4]["hotelPolicyInfo"])
    other_info = pd.io.json.json_normalize(hotel[5]["hotelOtherInfo"])
    return pd.concat([basic_info, rating_info, facilities_info, policy_info, other_info], axis=1)

def json_to_pd(file):
    """
    jsonファイルを読み込んでpandasにして返す
    """
    # 読み込み
    with open(file, "r", encoding="utf-8") as f:
        dict_data = json.load(f)

    # ホテル情報のリストを取得
    hotels = dict_data["hotels"]

    # 各ホテルをdataframeにして結合
    df_list = [create_df(hotel) for hotel in hotels]
    df = pd.concat(df_list)
    df["page"] = dict_data["pagingInfo"]["page"]
    return df


if __name__ == "__main__":
    # 並列数
    max_workers = 8

    # 処理するファイルリスト
    file_list = Path("./output/").glob("*.json")

    # json読み込みの並列処理
    with Pool(max_workers) as p:
        df_all = p.map(json_to_pd, file_list)

    # jsonファイルを順に読み込んで結合（並列しない場合）
    #df_all = [json_to_pd(file) for file in file_list]

    # 結合
    df_output = pd.concat(df_all)

    # 出力
    df_output.to_csv("./output/hotel_info.tsv", sep="\t", index=False, encoding="utf-8")
    df_output.to_pickle("./output/hotel_info.pkl")
